/* Task 26

Write a JavaScript class called CookieManager with 3 methods.

setCookie(name, data);
Set a cookie with the name as given argument and assign data given as argument. The cookie must expire in 15 minutes from the time it was set

getCookie(name);
Return the value of a cookie based on the name given as an argument.

clearCookie(name);
Delete a cookie based on the name given as an argument.

*/

class CookieManager {

    static setCookie(name, data) {
        const MINUTES_TO_MILLI_SECS = 60e3;
        let date = new Date(Date.now() + 15 * MINUTES_TO_MILLI_SECS);
        document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(data) + ";" + "expires=" + date.toUTCString();
        console.log(`Cookie set to "${name}"="${data}"`);
    };

    static getCookie(name) {
        let cookies = document.cookie.split("; ").map(cookie => cookie.split("="))
            .reduce((acc, [key, value]) => ({ ...acc, [key]: decodeURIComponent(value) }), {});
        return cookies[name];
    };

    static clearCookie(name) {
        document.cookie = encodeURIComponent(name) + "=; expires='Thu, 01 Jan 1970 00:00:00 UTC';";
        console.log(`Cookie "${name}" cleared.`)
    };

};
